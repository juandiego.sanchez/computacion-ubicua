<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Recibir el dato enviado desde la ESP32
    $dato = $_POST["postData"];

    // Realizar cualquier acción con el dato recibido
    // Por ejemplo, guardar el dato en una base de datos o mostrarlo en pantalla
    echo "Dato recibido desde ESP32: " . $dato;
} else {
    // Manejar el caso en que la solicitud no sea POST
    echo "Error: Se esperaba una solicitud POST";
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://bootswatch.com/5/lux/bootstrap.min.css" rel="stylesheet">
    
    <title>Enviar Datos desde ESP32</title>
</head>
<body>
<nav class="navbar navbar-expand-lg bg-primary" data-bs-theme="dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Grupo 1</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  </div>
</nav>
<h1>Recibir datos desde ESP32</h1>
    <form action="#" method="post">
        <label for="dato">Dato a enviar:</label>
        <input type="text" name="dato" id="dato">
        <input type="submit" value="Enviar">
    </form>
</body>
</html>

