#include <WiFi.h>
#include <HTTPClient.h>

const char* ssid = "TuSSID";            // Nombre de tu red Wi-Fi
const char* password = "TuContraseña";  // Contraseña de tu red Wi-Fi

const char* serverURL = "http://tudominio.com/ruta/tu_archivo.php";  // URL del servidor en DonWeb

void setup() {
  Serial.begin(115200);

  // Conéctate a la red Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Conectando a la red Wi-Fi...");
  }
  Serial.println("Conexión Wi-Fi exitosa");
}

void loop() {
  // Datos que deseas enviar al servidor (puedes personalizarlos)
  String dataToSend = "temperatura=25&humedad=50";

  // Crea una instancia de HTTPClient
  HTTPClient http;

  // Establece la URL del servidor
  http.begin(serverURL);

  // Establece el tipo de contenido de la solicitud (en este caso, application/x-www-form-urlencoded)
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");

  // Realiza una solicitud HTTP POST con los datos
  int httpResponseCode = http.POST(dataToSend);

  // Si la solicitud se realizó correctamente, muestra la respuesta del servidor
  if (httpResponseCode > 0) {
    String response = http.getString();
    Serial.println("Respuesta del servidor: " + response);
  } else {
    Serial.println("Error en la solicitud HTTP");
  }

  // Libera los recursos
  http.end();

  delay(5000);  // Espera 5 segundos antes de realizar la siguiente solicitud
}
