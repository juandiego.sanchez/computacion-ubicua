# Proyectos de Computación Ubicua

Este repositorio contiene ejemplos y proyectos relacionados con Computación Ubicua. Estos proyectos incluyen código para dispositivos como el ESP32, aplicaciones web, sensores y más. Cada proyecto se centra en diferentes aspectos de la ubicuidad y puede utilizarse como punto de partida para desarrollos futuros.

## Contenido

- [Medición de Temperatura con ESP32 y Sensor DHT11](./Medición%20Temperatura/): Ejemplo de cómo medir la temperatura y la humedad utilizando un ESP32 y un sensor DHT11.

- [Transmisión de Datos a un Servidor Web](./Envio%20Datos%20Servidor/): Código para enviar datos desde un ESP32 a un servidor web, incluyendo ejemplos de integración con DonWeb.

- [Desarrollo de Aplicaciones para WebOS](./WebOS/): Ejemplos y guías para desarrollar aplicativos en la plataforma WebOS.

- [Servidor web hosteado en ESP32](./Servidor%20Web%20ESP32/): Ejemplos y guias para desarrollar servidor web hosteado en ESP32

## Requisitos

- Placa ESP32 (para proyectos relacionados con ESP32).
- Conexión a Internet (para proyectos de transmisión de datos a un servidor web).

## Contacto

Si tienes alguna pregunta o sugerencia, no dudes en ponerte en contacto con el autor:

Juandiego Alexander Sánchez
juandiego.sanchez1218@gmail.com

¡Gracias por explorar nuestros proyectos de Computación Ubicua!
