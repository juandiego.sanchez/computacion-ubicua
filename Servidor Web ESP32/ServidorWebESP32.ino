#include <WiFi.h>
#include <DHT.h>

#define DHTPIN 2
#define DHTTYPE DHT11
#define LED 15 // Pin del LED

const char* ssid = "papa";          // Nombre de tu red WiFi
const char* password = "mobilpapa";   // Contraseña de tu red WiFi
const char* nom = "ESP32 WROOM ";

DHT dht(DHTPIN, DHTTYPE);

WiFiServer server(80);
WiFiClient client;

bool ledState = LOW; // Estado inicial del LED (apagado)

void setup() {
  Serial.begin(115200);
  Serial.println(F("Iniciando el sistema"));
  dht.begin();

  Serial.print("Conectando a ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(F("."));
  }

  server.begin();
  Serial.println();
  Serial.println(F("ESP32Wifi inicializado"));
  Serial.print(F("Dirección IP: "));
  Serial.println(WiFi.localIP());

  pinMode(LED, OUTPUT); // Configura el pin del LED como salida
  digitalWrite(LED, ledState); // Establece el estado inicial del LED
}

void loop() {
  delay(2000);
  float h = dht.readHumidity();
  float t = dht.readTemperature();

  if (isnan(h) || isnan(t)) {
    Serial.println(F("Error al leer el sensor DHT11"));
  } else {
    Serial.print(F("Humedad: "));
    Serial.print(h);
    Serial.print(F("% Temperatura: "));
    Serial.print(t);
    Serial.println(F("°C"));
  }

  WiFiClient client = server.available();
  if (client) {
    while (client.connected()) {
      if (client.available()) {
        String request = client.readStringUntil('\r');
        Serial.println(request);
        handleRequest(request);
      }
      webpage(client);
      break;
    }
    client.stop();
  }
}

void handleRequest(String request) {
  if (request.indexOf("/dig0on") > 0) {
    digitalWrite(LED, HIGH);
    ledState = HIGH; // Actualiza el estado del LED a encendido
  }
  if (request.indexOf("/dig0off") > 0) {
    digitalWrite(LED, LOW);
    ledState = LOW; // Actualiza el estado del LED a apagado
  }
}

void webpage(WiFiClient client) {
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println();
  client.println("<!DOCTYPE HTML>");
  client.println("<html>");
  client.println("<head>");
  client.println("<title>ESP32 Web Controller -Profe Z</title>");
  client.println("<meta name='viewport' content='width=device-width, initial-scale=1'>");
  client.println("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css'>");
   client.println("<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>"); // Incluye FontAwesomeclient.println("</head>");
  client.println("<body>");

  client.println("<div class='container mt-5'>");
  client.println("<h1 class='text-center'>ESP32 Web Controller- Profez</h1>");
  
  // Imagen del bombillo para mostrar el estado del LED
  client.println("<div class='text-center mt-4'>");
  client.println("<h3>Control del LED:</h3>");
   // Icono de bombillo de FontAwesome
  if (ledState == HIGH) {
    client.println("<p>Encendido</p>");
     client.println("<i class='fa fa-toggle-on  fa-5x'></i>");

  } else {
    client.println("<p>Apagado</p>");
    client.println("<i class='fa fa-toggle-off  fa-5x'></i>");
  }
  client.println("</div>");
  // Datos del DHT11 con estilo Bootstrap
  client.println("<div class='row mt-4'>");
  client.println("<div class='col-md-6'>");
  client.println("<h3>Temperatura:</h3>");
  client.print("<p class='display-4'>");
  client.print(dht.readTemperature());
  client.println(" &#8451;</p>");
  client.println("</div>");
  client.println("<div class='col-md-6'>");
  client.println("<h3>Humedad:</h3>");
  client.print("<p class='display-4'>");
  client.print(dht.readHumidity());
  client.println(" %</p>");
  client.println("</div>");
  client.println("</div>");

  // Botones para controlar el LED
  client.println("<div class='row mt-4'>");
  client.println("<div class='col-md-6'>");
  client.println("<a href='/dig0on' class='btn btn-success'>Encender</a>");
  client.println("</div>");
  client.println("<div class='col-md-6'>");
  client.println("<h3>&nbsp;</h3>");
  client.println("<a href='/dig0off' class='btn btn-danger'>Apagar</a>");
  client.println("</div>");
  client.println("</div>");

  client.println("</div>"); // Cierre del contenedor

  client.println("</body>");
  client.println("</html>");
}