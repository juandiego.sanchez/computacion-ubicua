#include <Adafruit_Sensor.h>
#include <DHT.h>

#define DHTPIN 4          // Pin al que está conectado el sensor DHT11
#define DHTTYPE DHT11     // Tipo de sensor (DHT11 en este caso)

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(115200);
  dht.begin();
}

void loop() {
  delay(2000);  // Espera 2 segundos entre lecturas

  float temperature = dht.readTemperature();  // Lee la temperatura en grados Celsius
  float humidity = dht.readHumidity();        // Lee la humedad relativa

  if (isnan(temperature) || isnan(humidity)) {
    Serial.println("Error al leer el sensor DHT11. Inténtalo de nuevo.");
  } else {
    Serial.print("Temperatura: ");
    Serial.print(temperature);
    Serial.println(" °C");
    Serial.print("Humedad: ");
    Serial.print(humidity);
    Serial.println(" %");
  }
}
